import React, { useState } from "react";
import { StyleSheet, Text, TouchableOpacity, View } from "react-native";

const App = () => {
  const [shouldShow, setShouldShow] = useState(false);
  const [textShow, setTextShow] = useState('Pokaż');

  function functionCombined(){
    setShouldShow(!shouldShow);
    if(shouldShow == true){
      setTextShow("Pokaż");
    }
    else{
      setTextShow("Ukryj");
    }
  }

  const onPress = () => functionCombined()

  return (
    <View style={styles.container}>
      <View style={styles.textContainer}>
        <Text style={styles.baseText}>Zadanie 2</Text>
      </View>
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
      >
        <Text>{textShow}</Text>
      </TouchableOpacity>
      <View style={styles.textContainer}>
        {shouldShow ? (
            <Text>Nazywam się</Text>
        ):null}
        {shouldShow ? (
            <Text style={styles.nameText}>Artur Hamernik</Text>
        ):null}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
        flex: 1,
        justifyContent: "center",
        paddingHorizontal: 10
      },
  button: {
    alignSelf: "center",
    width: 60,
    alignItems: "center",
    backgroundColor: "#DDDDDD",
    padding: 10
  },
  textContainer: {
    alignItems: "center",
    padding: 10
  },
  baseText: {
    fontSize: 18
  },
  nameText: {
    fontWeight: "bold"
  }
});

export default App;
